const { app, BrowserWindow } = require('electron');
const DiscordRPCClient = require('discord-rich-presence')('409399257586728961');
const path = require('path');
const url = require('url');

if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength,padString) {
        targetLength = targetLength>>0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength-this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength/padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0,targetLength) + String(this);
        }
    };
}

let mainWindow;

function createWindow()
{
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 500,
    resizable: false,
    titleBarStyle: 'hidden'
  });

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, "index.html"),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
  
  mainWindow.on('ready-to-show', () => {
	  mainWindow.show();
  });
  
  setActivity();
  
  setInterval(function() {
	  setActivity();
  }, 15e3);
}

app.on('ready', createWindow);
app.on('window-all-closed', () => {
  app.quit();
});
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

const thetime = new Date();

async function setActivity() {
  if (!DiscordRPCClient || !mainWindow) return;

  var playCount = await mainWindow.webContents.executeJavaScript("Runner.instance_.playCount");
  var highScore = await mainWindow.webContents.executeJavaScript("Runner.instance_.distanceMeter.getActualDistance(Runner.instance_.highestScore)");
  var distanceRan = await mainWindow.webContents.executeJavaScript("Runner.instance_.distanceMeter.getActualDistance(Runner.instance_.distanceRan)");

  DiscordRPCClient.updatePresence({
	  state: `On level ${playCount}`,
	  details: `Points: ${distanceRan}`,
	  startTimestamp: thetime,
	  largeImageKey: 'dino',
	  largeImageText: `High score: ${highScore}`,
	  instance: true
  });
}

process.on('unhandledRejection', (promiseRejection) => {
  console.error(`[Unhandled Promise Error] ${promiseRejection.stack}`);
});
