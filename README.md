# Dino
The Chrome dinosaur game except in Electron and with Discord Rich Presence support.

## How to setup

You need the following installed on your system to run this:

- Node.js (I use version 7.9.0)
- npm (I use version 5.5.1)

Run the following commands in your terminal:
```
$ git clone https://github.com/CuriosityDev/Dino.git
$ cd Dino
$ npm run start
```

This will install all Node.js dependencies and then run the app for you. The app will start in a new window.

In the future, I plan to allow you to build executables for your system, but for now we're stuck with this.